#02. BIOLOGY ZERO

**Biology Zero**

![](./assets/week2/1.jpg)

![](./assets/week2/2.jpg)


We have covered materials/introductions in: Synthetic biology (application of engineering principles to biology), Molecular Biology (deals with essential macromolecules such as Protein, Carbs etc) and Biochemistry (deals with the processes within and relating to living organisms).

We have learned that through Genetic engineering one can edit the DNA, especially through the development of CRISPR-CAS9 which is a unique technology that enables geneticists and medical researchers to edit parts of the genome by removing, adding or altering sections of the DNA sequence (this process has been developed by two women).
CRISPR-CAS9 has reduced previous problems in gene editing, there is an improved rate of about 70%.

Examples of gene editing in pigs has been shown, how they are made bigger in order to produce larger quantities of meat, and on the other hand other breeds of pigs have been edited into fitting the characteristics of a pet. Also organ transfers from animals (especially pigs I believe) to humans will be possible in the near future. These modifications are widely spread in crops as well.

Through gene editing the human lifespan can increase, which poses another moral question, if humankind actually should explore that option.

We have learned that we can use technology today to actually “bio print human blood vessels” and many other processes are in development. https://organovo.com/

We have had the opportunity for four days to conduct experiments in the lab and understand the basics of working in a sterile environment, cultivating bacteria and most importantly given the task to create a hypothesis on a topic that interests us based on the information we were exposed to throughout the week.
I identified my areas of interest on the third day of the week and started researching topics on environment/pollution/microbes/mycelium.

![](./assets/week2/3.jpg)

![](./assets/week2/4.jpg)

I had an idea of incorporating microbes that feed off of air pollution into facades of buildings and my hypothesis was something along the lines “Microencapsulation of organisms into facades will improve the air quality by turning buildings into pollutant absorbents.”
Turns out scientists have been researching that field for at least two decades and various developments have been made but an efficient formula/preservation of the bacteria into the façade remains to be developed. While I was researching the topic I came across various articles about bacteria that are able to absorb pollution (including plastic) and how scientists and researchers are finding new ways to put these discoveries into use. Besides bacterias into facades, three areas of interest came up during my research:

**1. Bacteria into Facades**

**Resources**

Architects of nature: growing buildings with bacterial biofilms
Martyn Dade-Robertson,  Alona Keren-Paz, Meng Zhang and Ilana Kolodkin-Gal

**2.	Bacteria in Soil and Plants**

Usage of bacteria and plants that can remediate pollution through phytoremediation.
(Phytoremediation refers to the technologies that use living plants to clean up soil, air and water contaminated with hazardous contaminants.)

**Resources**
https://www.frontiersin.org/articles/10.3389/fpls.2017.01318/full

"Application of microorganisms in bioremediation-review"
Endeshaw  Abatenh, Birhanu Gizaw, Zerihun Tsegaye, Misganaw Wassie

"Trees harness the power of microbes to survive climate change"
Jennifer A. Lau, Lay Tm Lennon and Katy D Heath

**3. Bacteria in Water/Petroleum/Plastic**

**Resources**
"Microbial Degradation of Petroleum Hydrocarbon Contaminants"
http://dx.doi.org/10.4061/2011/941810


![](./assets/week2/5.jpg)
“Some bacteria think plastic is fantastic”
Science, this issue p. 1196; see also p. 1154

There are challenges into encapsulating the bacteria in order to save its properties and keep it alive but advancements have been made in this field according to the research papers above. This is a field that interests me and might be somehow incorporated into one of my projects.

![](./assets/week2/6.jpg)

**Interesting developments**

In recent years Biohacking has become a popular way of introducing biology to non-scientists and allow them to conduct experiments. Apart from the non-scientific community that benefits from “hacking” approaches to biology, the scientific community uses these biohacking techniques for several reasons including low cost operations and usually less bureaucracy in procedures. There are several websites that show how to use ordinary kitchen appliances to conduct experiments and what kind of ingredients can be used.
1. https://diybio.org/
2. https://www.syntechbio.com/

In addition we have had interesting guest lectures as well from Mette Bak Anderson, Thora Ha, and X how they are combining biology and design.
Mette has made some valid points regarding material usage and the design process, below are some quotes from her lecture.
“We need to change how the designer thinks, the products need to be designed for circular life”
“The designer has to know and understand where the materials are coming from and know the materials they are working with”
She has given us insight into very interesting materials such as Mycellium, Nettle, Hemp, Spider Silk, honeybee silk, and many other innovative approaches being used to develop new sustainable materials.

Thora Ha has introduced to us some very interesting projects that she and her team have produced over the years, those projects can be found at https://www.thoraha.com/
The project regarding usage of bacteria into concrete cracks was specifically of interest to me.


Overall Biology Week was interesting and quite captivating, I came to be very curious about the topics mentioned above and will be looking forward towards learning more about the related issues. I have already established contacts with peers who are interested in these fields and am looking forwards towards exploring possible projects together.

**Moments during BiologyZero**

![](./assets/week2/BiologyZ1.jpeg)

![](./assets/week2/BiologyZ1.jpeg)


![](./assets/week2/BiologyZ2.jpeg)

![](./assets/week2/BiologyZ3.jpeg)

![](./assets/week2/BiologyZ4.jpeg)

![](./assets/week2/BiologyZ5.jpeg)

![](./assets/week2/BiologyZ6.jpeg)

![](./assets/week2/BiologyZ7.jpeg)

![](./assets/week2/BiologyZ8.jpeg)

![](./assets/week2/BiologyZ9.jpeg)

![](./assets/week2/BiologyZ10.jpeg)

![](./assets/week2/BiologyZ11.jpeg)

![](./assets/week2/BiologyZ12.jpeg)

![](./assets/week2/BiologyZ13.jpeg)

![](./assets/week2/BiologyZ14.jpeg)

![](./assets/week2/BiologyZ15.jpeg)

![](./assets/week2/BiologyZ16.jpeg)

![](./assets/week2/BiologyZ17.jpeg)

![](./assets/week2/BiologyZ18.jpeg)

![](./assets/week2/BiologyZ19.jpeg)

![](./assets/week2/BiologyZ20.jpeg)

![](./assets/week2/BiologyZ21.jpeg)

![](./assets/week2/BiologyZ22.jpeg)

![](./assets/week2/BiologyZ23.jpeg)

![](./assets/week2/BiologyZ24.jpeg)

![](./assets/week2/BiologyZ25.jpeg)

![](./assets/week2/BiologyZ26.jpeg)

![](./assets/week2/BiologyZ27.jpeg)

![](./assets/week2/BiologyZ28.jpeg)

![](./assets/week2/BiologyZ29.jpeg)

![](./assets/week2/BiologyZ30.jpeg)

![](./assets/week2/BiologyZ31.jpeg)

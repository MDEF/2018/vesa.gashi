WEEK 4

![](./assets/week4/cover.jpg)


**Exploring Hybrid Profiles Week**
October 23rd

During this week I have managed to visit only one workshop, that is of Sara (de Ubieta) and was left in awe of her approach.
I appreciated her attitude towards choosing a career path, in what stroke me the most was that not only did she pursue a second passion (just after finishing a highly competitive/demanding degree) but she also gave herself time to understand and explore her new field of interest. She explained how she has not followed up on several business opportunities and wants to focus on researching the field calmly.

Coming from a business/entrepreneurial background these type of approaches fascinate me, when people dive into the uncertainty following a passion and being relaxed about it along the way. For me the financial sustainability of the venture would be my first concern and missing on opportunities that would offer me some sort of financial stability in the long run would be a nightmare. For Sara, based on her answers, that wasn’t her main concern. She invested time and effort, had passion for her work and although she didn’t follow through with every opportunity that came along, she managed to turn her passion into a successful venture anyway.

I don’t know if Saras attitude will stick on me and I don’t know if that’s the right approach for me but it certainly was an eye opener that there are people who deal with situations very differently from how I would’ve handled it.

Apart from that, I loved the crafting part of the class so much that for a moment I thought if I should start making shoes for a living.
In general Sara is an inspiring person and will look forward towards maintaining a relationship with her.

Process of Shoe making
![](./assets/week4/Shoe.jpeg)

![](./assets/week4/Shoee.jpeg)


**Personal Development Plan**
October 25th

I am excited to research the topics which I love and learn about new fields and creating new experiences throughout the following weeks.

My areas of interest are to learn more about Participatory Design, Design and Function of Public Spaces, The effects of public spaces on the functioning of communities, Circular Economy, Interactive Installations etc.

**Personal Project**
October 25th

I am intrigued by the usage of public spaces and how it impacts communities around it. I want to focus my project in helping communities find mutually beneficial interests that would help them connect with one another and foster better relationships.

My concerns regarding improving the way communities interact amongst each other within neighborhoods were always grounded around the community I grew up in and all the solutions I think of revolve around it. At this point I have been having dilemmas to as if I should focus on that community that I know so well or if I should focus my work in possible projects in Barcelona. It is because of this matter that I have been procrastinating in identifying where I would like to focus my project and categorize the key components that come with it. Also, ongoing research and discussions with peers have kept me shift my areas of research.

So far my research and this assignment have undergone three drafts/concepts regarding public space and where I need to focus my research for the project.

**Draft 1**
October 26th

I want to help communities find mutually beneficial interests that would help them connect with one another and build better relationships with each other.
One alternative to doing so would have to be some sort of win-win activities/interests that would mean gaining either financially, self-growth, culturally etc.
Other important actions would include interventions in public spaces that in some way would foster interaction with each other, create some sort of community value etc.
Things I like and I believe could be integrated as part of the project:
-Using power trees
-Circular economy (growing their own food, bartering, etc.)
-Create feeling of belonging to the community, connect with one another
For those communities/individuals that are not inclined to partake in community building activities or use public spaces, one way to making them do so is by offering incentives depending on the category, e.i by age groups. Students would get extra credit/smth at school for participating in community work or projects related to these events, the middle age would get maybe a tax break or lower taxes on property/recognition by mayor etc.

**Draft 2**
October 31st

Regardless of whether I chose to work with a neighborhood/area to my home country or elsewhere, there are two important things I will have to take into account, one is the current physical state/constraints that an area poses and the second one is the mentality of that community. The first one to my opinion is easier and has less moving parts in order for it to be successfully implemented but the second one might have a counter effect if the project doesn’t address some of the key characteristics that define a certain community.

Below I will give a simple example to illustrate one characteristic of a certain neighborhood I have stumbled upon during my walks to Elisava which has made me think on the way I should approach the act of designing when thinking about public spaces.

In general public spaces in Barcelona are vast (compared to both small and major cities in Europe), they are suitable and based on my observations I believe serve their intended purpose.
However, there are some interesting ways how some of these spaces are being utilized by certain categories. There are small blocks within central neighborhoods in Barcelona which are mostly occupied by Middle Eastern families, which according to my observations make use of the facilities and public spaces provided but not both genders at the same time. Almost identical parks exist in several neighborhoods but are not being utilized in the same way. In these specific neighborhoods I’m referring to, it’s either the women with kids playing in the parks or taking part in activities during the day, or men hanging out and doing the same during evenings. So far I haven’t encountered those types of communities partaking in any activities in public spaces where both genders are mixed, which poses several questions and concerns. One concern is that when designing for public spaces, by being respectful to the communities’ lifestyle chances are that those behaviors will only be cemented further if they have a suitable platform to cultivate their actions.

From this perspective some issues arose, should one take into account the mentality/characteristics of that community when searching for solutions?  Should we design to serve the purpose of those communities, design to change the way they act or design because we believe in it and let it be used by those to whom it is appealing?

Note: My observations are far from scientific and I do not have any official data to back up my claims, but the behavior of that community has caught my eye on several occasions without me even intending to observe that.

Note: Possible area for research “Utilization of public spaces/infrastructure by different communities/ethnicities”

**Draft 3**
November 6th

This draft is still under development, I have to research a lot more on this topic but it certainly is an area I would like to explore and try to understand better how to create efficient multipurpose public spaces that help foster relationships amongst communities. This topic has been influenced by discussions with like-minded peers regarding the usage of space and how different cultures experience it. Fine examples would be how Eastern cultures view and use space in general versus how the Western cultures do and their emotional attachment to it. The way space is used in rotation in Japan, especially inside buildings is really interesting and I will have to explore this topic further.  

Areas which I would like to research more in the following days. Can public spaces be multifunctional and be put into use in rotation? Different functions in daytime vs nighttime? Can public spaces serve as common areas where one performs private tasks?

**Implementing the project**

If my research proves to be complex and there is not enough time to find a way to integrate my findings into my project, I already have a project in mind which I can work on.
In my hometown, construction development has been very hectic after the war and up until recently without any regulations whatsoever. There are entire neighborhoods that have been developed, that house tens of thousands of citizens that haven’t followed any urban planning whatsoever. The result is a chaotic living space with congested traffic, almost non existing urban transport and lack of sidewalks, lack of public spaces, lack of parks, heavy pollution and many other issues that affect the quality of life of these citizens.

Initially I wanted to take this neighborhood as a case study and see how certain interventions can be made to improve the functioning and the quality of life for those citizens, with an emphasis on creating public spaces that would serve as a basis for better community building.

If I decide to focus on this project, depending on the data that the local authorities have, thorough research will be needed to be done which I believe will require external funding. Also, the magnitude of the project that I initially had in mind would require a team of people working on it, so it might be an interesting opportunity to integrate others interested from the class to implement it. If not, I could focus on a narrower aspect of the project and implement it on my own.


**How have the 3 Project reads in class influenced us?**

Designing for the unknown: A design process for the future generation of highly interactive systems and products
To me this was the most impressive article from others that were discussed in class. This article talked about the design processes. It basically explores the traditional ways of exploring the design process:
The first method being the Rational Problem Solving that choses a rational problem solving process that implements information, the second method being the Reflective Method that is focused in generating information. The third method, is a new approach to the design process “The reflective transformative design process” that supports flexibility and individuality, the central activity being “ideating, integrating and realizing interaction solutions between users and systems in a context of use”. It basically encourages going back and forth between researching and testing the product before finding the ideal solution.
Until today, as far as my experience goes I use most of the techniques in real life as well and have proved to be the best tools and methods for me, although the approach depends a lot of times on  the circumstances and you cannot apply the same methods every time. To me it’s important to understand when you have the flexibility to explore and when you have to deliver something quick and efficiently.

**Extra Learning Activities**

I think it would be interesting to a add a reading class, where based on our areas of interest we could be guided to the materials/books we should be reading and perhaps once in every two weeks we can have a class discussion on what everyone is reading. It would be a could way to utilize our time and the resources each of us has.

Also, it would be good if we can start having suggestions from faculty on tailored visits or areas of exploration based on our individual interests. If I am interested in social cohesion, community building, public space etc. I would love to have suggestions on case studies/books to read, interesting people/groups to meet etc.

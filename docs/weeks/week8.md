#08.

**Material Speculation Notes**

![](./assets/week8/3.jpg)

**Day 1**
Challenging the future. Prototyping without technology being available about a product or service in the future.

First day of the Material Speculation class, as by now most of these classes on their first day, evoked confusion and I wasn’t sure if I am grasping the concept as I should. We did an exercise to prototype a product, chosen from a pile of everyday objects we brought to class, whose usage we were to alter to another function different from its original intent.
I picked a wooden spoon.

My idea was what in the near future humans would not need to consume edibles as a form of producing energy but rather feed themselves through photosynthesis (hint: biology zero influence class). In that future we would not be needing traditional cutlery anymore but what if the sole function of the cutlery would be to use them as a symbol for getting together and share “a meal”. Instead of getting together to have a meal with someone, you would invite them to photosynthesize together. Using this concept and the idea of the wooden spoon as a symbol, I thought of adding a mirror to it. Making it an accessory for a faster recharge while photosynthesizing. In this case I had to add a mirror, but in traditional metal spoons one doesn’t need to add mirrors since they already are able to reflect the sun from their surfaces.

**Day 2**
We had a presentation of Angelas  work/projects.
The two highlighted projects where about the clothing line in space and the Vega wearable light technology. Interesting techniques used for her research, concepts and points of view.

After the presentation we experimented with the Chroma key app in class by wearing texture in green and taking videos or pictures with the app. Exhibits shown below:

There was a certain experience you got from the way when you were wearing the clothing/covering yourself, one somehow felt immersed in the fabric. One felt different as if you became another character or your alter ego. Mine was a taco. I was really into being projected somehow as a type of food, experimented with the pizza and spaghetti as well. Carbs are my guilty pleasure I guess.

While wearing the fabric you draw attention to yourself from others and at the same time you become a focus point. If you are a shy type it might be difficult to manage or it might even be offsetting to try it. These experiments in a way outstrip you.

Concepts discussed in class were also on how to confront yourself with fabric, live with it, experience it.
Can cloth produce energy? To have your phone charged while walking and some other ideas and concepts, overall interesting and unknown points of view to most of us.

**Day 3, 4 and 5**
We were asked to prototype a product with the elements brought to class by the tutor. This product should have a function in the future without us having the available technology. It was basically an exercise to get our imagination going on and it was wonderful, the result was both exiting and getting lost once more.
The tutor brought several items such as discarded wooden laser cut pieces, cardboards, plastic cups, plates, spoons, foil and many other items, he also laid down several pieces of paper containing the basic human needs on them and each of us was supposed to select one. I selected the word order.

I made a pair of glasses out of wood that would serve to project order upon things we look at through them. The next phase of the exercise was to pick another word and combine its function with the existing product you made. I picked the word Social Contact.

My idea was creating an environment through VR glasses where people from the neighborhood/same areas could spend time together in participating in activities such as sports, debating or other activities that would help build the community up. I was speculating with this idea for two days on how I could make it work and it seemed quite possible to build something like that. I really wanted to experiment with the VR glasses that IAAC has in possession but they did not allow me to try them on. All in all this week was interesting and it opened up our imaginations and made us get out of our comfort zones. To me it was dwelling into unknown territory too deep so I had to reevaluate my options and decisions regarding the project.

The last two days were quite nice we talked about our projects, our ideas with both the tutors, IAAC faculty and our classmates which was quite helpful and insightful. Overall a very interesting week that shifted our focus and shed light into paths most of us didn’t think would be interested to know more about.

![](./assets/week8/WhatsApp Image 2018-12-05 at 10.38.20.jpeg)
![](./assets/week8/WhatsApp Image 2018-12-05 at 10.38.20(1).jpeg)

My “Orderomatic” glasses and me wearing my “Paradox” glasses from day 3 and 4. (I really did not want to include this picture but we were told we need to document a picture of ourselves with the product, so voila.)

#01. MDEF BOOTCAMP


**Week 1/Blog #1**
08/10/2018
Barcelona

The first week of BootCamp has been a mixed bag of expectations and possibilities mostly accompanied by highs of excitement and rarely by dubious thoughts. My entire last minute experience coming to Barcelona and committing myself to a degree that was not yet structured was (and continuous to be) an adventure that has shook me and interfered with my usual processing abilities and all of this in itself is a completely new experience to me.

Thinking about these experiences the past three weeks, putting into account the pre-course as well, which to me in itself was an object made up of a hundred flying pieces that I am still trying to digest, I can say that there were times when I was dazed by the new information but also regarding the possibilities of creating and implementing thrilling projects.

One thing that I have learned for sure, which in itself encompasses everything learned during the past weeks, is that I definitely do not know much about technology or softwares and that an entire new chapter of prospects has been laid out in front of me. The fact that the possibilities are endless in the field of technology and innovation is something which one understands naturally but for the first time I have actually been confronted with the sheer idea of being part of that process.
Below I would like to share a few interesting points and areas which I enjoyed during the BootCamp week that I believe to be beneficial to the type of project I want to be a part of for the time being.

1)	I have skimmed through the Fab City project prior to my arrival in Barcelona, at first my impression was that it was a far-fetched over ambitious project that only sounded good (and a bit vague) on paper but hardly applicable. I guess this was mostly due to my own gloomy perspective and lack of trust in my countries public administrations ability to enforce any type of project of that kind, thus lack of trust that I would ever get to see it happen anywhere.

That being said, I am overly excited to learn more about it and am already seeing pieces that fit into the ideas of my project. A few interesting points that for some reason are embedded in my head now are:
- The fact that there is no data/statistics regarding the consumption/usage of products/goods within the cities. A fact which might be obvious on its own but interesting when you actually hear it being put into context. This I believe will somehow play an interesting role in shaping up my project.

- Crypto currencies, after several attempts of friends trying to explain how that functions, I cannot say that I still fully understand its concept but I am getting there. This I don’t know how it will fit in my project but I do know that crypto currencies are a very hot topic and that according to unofficial statistics (mostly rumors) Kosovo has some of the largest investors in crypto currencies in the Balkans. At least 1 in 5 people I know back home have invested money in it in one way or another for the past 2-3 years, so the crypto currencies can play a role in my project (maybe by using the topic in some way to stimulate the local population of implementing some type of the projects activity). It is a vague proposition but nonetheless one that I think will play a role.

- Urban farming, definitely something I am interested in.

2)	Smart Citizen app, a project I had no idea it was implemented in my hometown Prishtina. Was really astounded by that fact because for every single point mentioned during its presentation my inner reaction was “I HAVE TO implement this project back home!” and then seeing that it was actually tested there was really exciting. I am looking forward to seeing this developed further and will definitely want to be a part of this project and further enriching it with my input.

3) Safari day trip, was an interesting experience. All the workshops/projects in the area have something valuable to offer, especially regarding community building/empowerment. All of the labs we visited and many others that remain to be visited can and will play a role in shaping the structure of my project. It is really beneficial to see them being implemented, as they can serve as a basis for cooperation and modeling of similar activities in the communities I am interested to work in.

-Would like to highlight that the work they do at Transfolab is interesting and easily applicable in terms of the investments and equipment needed. Other than that, one will need great motivation to actually put it in motion but its something that I believe can shape up/add on to my project.

![](./assets/week2/IMG1.jpg)

![](./assets/week2/IMG1.jpg)


-The Super Block project, is also really interesting and would like to see how that will be further developed here in Barcelona. Also, would love to test it how that would work out in my hometown Prishtina, especially because of the lack of infrastructure regarding public spaces, which has also been a key point in my idea of the project so far.

Interesting to find out how there was a lack of public debate regarding its implementation and am curious to see how it will play out in the near future.

4) Leka restaurant and Indissoluble, were interesting to see. The open concept restaurant, something that did not cross my mind earlier, is a point taken and I already have some ideas on how to put it into good use.

As per the indissoluble, it was my favorite part since it involved what I actually do every day in my job, only at a larger scale. I suppose that the excitement also came from the fact that for once since I came here, I was confronted with something familiar to me. So that was a comforting experience 

“The brain” installation struck so many notes and rang so many bells in my head, I will definitely want to implement something similar.
Overall super excited to a possibility of cooperation with the Indissoluble team or even developing similar projects with my peers at IAAC.


5) An interesting point that Mariana made during a discussion was the fact that consumption doesn’t have to be a bad thing and that one product can be used to cultivate a byproduct, in terms of recyclability. That’s a concept I had not been exposed to and was an interesting point of view that has given me a different perspective to see consumption as an act altogether.

6) Tomas’s presentation. Still did not get to research the projects presented, but it’s a sea of opportunities. I have a side-project in my mind because of his presentation, but the topic remains to be further researched.

That’s a wrap on the experiences I could articulate thus far, there are many more which remain to be seen how they will shape up my ideas and beliefs.


Hasta la vista














![](assets/1.jpg)

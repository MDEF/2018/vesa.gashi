### About me

Ola!

I am Vesa Gashi currently an MDEF student at IAAC. Before my journey in Barcelona, I would describe myself as an entrepreneur that has experience in surviving in a third world economy.

Keywords that makeup my professional background: Marketing, PR, Production, Event Management, Diplomatic Protocol, Business Management, Ups and Downs, Success and (mostly) Failures.

Keywords that makeup my leisurely interests: Food (food, food, healthy food), Family, Friends, Fun, Flowers and Travel.

Keywords that makeup my interests regarding my project at MDEF: Social Change, Social Cohesion, Community building.

Apart from MDEF duties I am really hoping that by the end of the documentation process I will be able to do an entire post in Spanish.


Muchas Gracias for taking the time to read my blog.
